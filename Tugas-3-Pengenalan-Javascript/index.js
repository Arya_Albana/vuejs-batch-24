//Soal 1
var pertama = "saya sangat senang hari ini"
var kedua = "belajar javascript itu keren"
var tiga = pertama.substr(0, 4)
var empat = pertama.slice(12, 18)
var lima = kedua.substr(0, 7)
var enam = kedua.substr(8, 10)
var tujuh = enam.toUpperCase()
var res = tiga.concat(" ",empat, " ", lima, " ", tujuh)
console.log(res)

//soal 2
var kataPertama = "10"
var kataKedua = "2"
var kataKetiga = "4"
var kataKeempat = "6"

var kataPertama = Number("10")
var kataKedua = Number ("2")
var kataKetiga = Number ("4")
var kataKeempat = Number ("6")
console.log(kataPertama+kataKedua*kataKetiga+kataKeempat)


//soal 3
var kalimat = 'wah javascript itu keren sekali'
var kataPertama = kalimat.substring(0, 3)
var kataKedua = kalimat.substring(4, 14)
var kataKetiga = kalimat.substring(15, 18) 
var kataKeempat = kalimat.substring(19, 24)
var kataKelima = kalimat.substring(25, 31)

console.log('Kata Pertama: ' + kataPertama)
console.log('Kata Kedua: ' + kataKedua)
console.log('Kata Ketiga: ' + kataKetiga)
console.log('Kata Keempat: ' + kataKeempat)
console.log('Kata Kelima: ' + kataKelima)

var str1 = "Hello ";
    var str2 = "Nama Saya";
    var str3 = " Andi!";
    var res = str1.concat(str2, str3);
console.log(res)